<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'idigital');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|+5D;EGYOE%gQOVt8a5>9fSK7B{aN6akNlgyqh{>XMGYr?HCa2q-13h]E`;/UFmw');
define('SECURE_AUTH_KEY',  ')s_ Onf5tF>>l.lpuLU@qp69QvU2TN,9^fN5]B([}]D^ RA`q`9uB=qk1t3+/z<?');
define('LOGGED_IN_KEY',    'WmQ.`+ rnO!8>F)+pB3Y?4oAf{:a2vrm7lSn.Q^djDPw^(r*UE)O~NN&D{j~<x-d');
define('NONCE_KEY',        'IX Bs`*#</{f7TKR%.ZAVa6EI1CghwThI9.bl-snl4a&Nl^->;$Rqa?!J/_Nb5Yh');
define('AUTH_SALT',        ',$&U.za#o:[bLdGgR)|tKj%H91jGBU}79@} 7,c%,d#0Q~U`p@7Z1;@5x1-cz2(7');
define('SECURE_AUTH_SALT', 'dFI29cJW46VO{otLY(-hLOe&#@X;[|N+i,:M{cy-,dau/<v(FY>(ra.$p:~H14__');
define('LOGGED_IN_SALT',   ':SY.FAy+T_.fr-N ;3+p?(xWGk:{>F[;MI86AAo@)NXK/kg:kvAFYLi}0We(Z1)#');
define('NONCE_SALT',       '5./u/BlBX%fU46c4dm>ckv4ut>XdGu:R]8EnWojbG<wj)c={Xs Pxo(vrMQ^6oj:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
