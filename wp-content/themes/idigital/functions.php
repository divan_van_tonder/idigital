<?php
/**
 * All Theme Functions
 * @author Lightswitch Solutions
 */
require_once(get_template_directory() . '/includes/constants.php');

//turn off admin bar
show_admin_bar(false);

/**
 * Add support for various theme requirements
 * @action after_setup_theme
 */
function idigital_theme_setup() {
    //Let WordPress manage the document title.
    add_theme_support( 'title-tag' );	
    //Enable support for Post Thumbnails on posts and pages.
    add_theme_support( 'post-thumbnails' );
    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'primary' => esc_html__( 'Main Nav', 'idigital' ),
    ));
    //Switch default core markup for search form, comment form, and comments to output valid HTML5.
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));
    //Enable support for Post Formats.
    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
    ));
}
add_action('after_setup_theme', 'idigital_theme_setup');

/**
 * Enqueue scripts and styles.
 * @action wp_enqueue_scripts
 */
function idigital_scripts_styles() {
    wp_enqueue_style('theme-stylesheet', get_stylesheet_uri());
    wp_enqueue_style('theme-media-queries', get_template_directory_uri() . '/css/media-queries.css');
    if(is_singular() && comments_open() && get_option( 'thread_comments' )) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'idigital_scripts_styles');
/**
 * Add Google Analytics into footer
 * @action wp_footer
 */
function idigital_google_analytics() {
    echo '<script src="http://www.google-analytics.com/ga.js" type="text/javascript"></script>';
    echo '<script type="text/javascript">';
    echo 'var pageTracker = _gat._getTracker("' . get_option('idigital_google_code') . '");';
    echo 'pageTracker._trackPageview();';
    echo '</script>';
}
add_action('wp_footer', 'idigital_google_analytics');

/**
 * Theme Options/Settings Page
 * @action admin_menu
 */
function idigital_create_options_menu() {
    //create new top-level menu
    add_menu_page('Settings', 'Settings', 'administrator', __FILE__, 'idigital_settings_page', null); //replace null with plugins_url('/images/icon.png', __FILE__)
    //call register settings function
    add_action('admin_init', 'idigital_register_settings');
}
add_action('admin_menu', 'idigital_create_options_menu');
/**
 * Theme Options Page Include
 */
function idigital_settings_page() {
    include(get_template_directory() . '/theme-options.php');
}
/**
 * Register Settings/Options
 */
function idigital_register_settings() {
    register_setting('idigital-settings-group', 'idigital_google_code');
}
/**
 * Custom Login Logo
 * @action login_head
 */
function idigital_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image: url('.get_bloginfo('template_directory').'/screenshot.png) !important;width:280px !important;height:105px !important;background-size:280px !important;}
        h1 {background-color:#fff !important;  padding:30px 0 0 !important;}
        body.login {background:#fff !important;}
    </style>';
}
add_action('login_head', 'idigital_login_logo');

/**
* Get YouTube Video Thumbail
* @param <string> URL of video
* @return <string> ABS URL
*/
function idigital_get_youtube_thumbnail($video_url) {
   $youtube_vars = array();
   parse_str(parse_url($video_url, PHP_URL_QUERY), $youtube_vars);
   if(!isset($youtube_vars['v'])) {
       return FALSE;
   }
   $thumbnail_url = "http://img.youtube.com/vi/" . $youtube_vars['v'] . '/0.jpg';
   return $thumbnail_url;
}

/**
 * Generic Ajax Function
 * @action wp_ajax
 */
function idigital_ajax(){
    wp_send_json(array());
}
add_action('wp_ajax_nopriv_action', 'idigital_ajax');
add_action('wp_ajax_action', 'idigital_ajax');

/**
 * Add global JS variables to the page
 * @action wp_head
 */
function idigital_global_js_variables() {
?>
<script type="text/javascript" language="javascript">
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}
add_action('wp_head', 'idigital_global_js_variables');