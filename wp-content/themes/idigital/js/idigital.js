$(window).on( "load", function() {
	$(".loading").delay(500).fadeOut(500);
	/** ODOMETER **/
	$(window).scroll(function() {
		var screenTop = $(window).scrollTop();
	  var odometer = $("#fourthsection").offset().top - 700;
	  // console.error(screenTop + " >= " + odometer);
	  if(screenTop >= odometer) {
			$('#odometerone').html($('#odometerone').data('value'));
			$('#odometertwo').html($('#odometertwo').data('value'));
			$('#odometerthree').html($('#odometerthree').data('value'));
	  }
	});
});
$(document).ready(function() {
	$(".button-collapse").sideNav();
  $('#fullpage').fullpage({
	  anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourthPage', 'lastPage'],
	  menu: '#myMenu',
	  afterLoad: function(anchorLink, index){
	  	$( "#myMenu li:nth-of-type("+index+")" ).addClass("activeli");
	  },
	  afterRender: function () {
	  	if ( $('video')[0]) {
	            $('video')[0].play();
          }
        },
	  onLeave: function(index, nextIndex, direction){
      var leavingSection = $(this);
      //after leaving section 2
      if(index == 2 && direction == 'up'){
       	$('.header').addClass('transparent');
       	if ( $('video')[0]) {
	            $('video')[0].play();
          }
      }
      if(index != 1 && direction == 'down'){
       	$('.header').removeClass('transparent');
      }

      $( "#myMenu li").removeClass("activeli");
      $( "#myMenu li:nth-of-type("+nextIndex+")" ).addClass("activeli");
	  }
	});
	// Slick JS
	$(document).on('ready', function() {
	  $(".slick-slider").slick({
	    variableWidth: true,
	    centerMode: true,
	    infinite: true,
	    autoplay: true,
	    autoplaySpeed: 0,
  	    speed: 1000,
  	    cssEase: 'linear',
	    slidesToShow: 5,
	    slidesToScroll: -1
	  });
	});

	$(".case-study-text").on('mouseenter', function(){
		$(this).find(".picture-overlay").fadeIn(300);
	});
	$(".case-study-text").on('mouseleave', function(){
		$(this).find(".picture-overlay").fadeOut(300);
	});

	if ($(window).width() < 600) {
		$(this).find(".picture-overlay").hide();
			$(".case-study-text").on('click', function(){
				$(this).find(".picture-overlay").fadeIn(300);
			});
		}
	
	/* MAKE HEADER DARK ON UPSCROLL */
	$(window).bind('mousewheel', function(event) {
		if (event.originalEvent.wheelDelta >= 0) {
			$(".header").addClass("dark-header");
			console.log('Scroll up');
		}
		else {
			$(".header").removeClass("dark-header");
			console.log('Scroll down');
		}
	});
	
	$("#nav-trigger").on('click', function() {
		var hasWidth = $('.header').attr('style');
		(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
		var viewportWidth = $(window).width();
		if (typeof hasWidth !== typeof undefined && hasWidth !== false) {
  		$('.header').removeAttr('style');
  		$('.site-wrap').removeAttr('style');
		} else if (viewportWidth < 600) {
			$(".site-wrap").css({'right': viewportWidth});
			$('.header').css( {'width' : $('.header').width() - viewportWidth });
		}	else {
			$(".site-wrap").css({'right': 400});
			$('.header').css( {'width' : $('.header').width() - 400 });
		}	
	});

	var fpnavheight = $('#fp-nav').height();
	var fpnavmargin = (fpnavheight/2);
	$('#fp-nav').css('margin-top', -fpnavmargin);
	/* Hide fullpage nav squares when menu opens */
	$('button.c-hamburger').click(function(){
    $('#fp-nav').fadeToggle(300);
	});
	new WOW().init();
});