<?php
/**
 * Template name: Case Study
 *
 */
get_header(); ?>
<!-- FIRST PAGE -->

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'full'); ?>


<div style="height: 100vh; width:100vw; background:transparent; position:relative;" class="section valign-wrapper bg-wrapper">
	<?php $video_url = get_post_meta(get_the_ID(), 'wpcf-bg-video', 1); ?>
	<?php $image_url = get_post_meta(get_the_ID(), 'wpcf-bg-image', 1); ?>
	<?php if ($video_url != "") { ?>
		<video autoplay id="bgvid" loop>
			<source src="<?php echo $video_url; ?>" />
		</video>
	<?php } elseif ($image_url != "") {?>
		<img class="video-placeholder" src="<?php echo $image_url; ?>">
	<?php } else {?> 
		<img class="video-placeholder" src="<?php echo $image[0]; ?>">
	<?php } ?>

	<div class="container">
		<div class="row">
			<div class="col l8 offset-l2 valign s12">
				<?php 
					if ( have_posts() ) : 
						while ( have_posts() ) : the_post(); 
				?>
				<?php $text_color = get_post_meta(get_the_ID(), 'wpcf-text-color', 1); ?>
					<h1 style="color:<?php echo $text_color; ?>;"><?php the_title(); ?></h1>
				<?php
					    the_content();
					  endwhile; 
					endif;
				?>
			</div>
		</div>
	</div>
	<div class="row down-arrow">
		<div class="col l12 s12">
			<a href="#nextsection">
				<img src="/wp-content/themes/idigital/images/chevron-down.svg">
			</a>
		</div>
	</div>
</div>
<!--  END FIRST PAGE -->

<!-- SECOND PAGE -->
<div class="section" id="nextsection">
	<?php 
		$brand_overview_raw = get_post_meta(get_the_ID(), 'wpcf-brand-overview', 1); 
		$brand_overview = apply_filters('the_content', $brand_overview_raw);
	 ?>
	<!-- BRAND INFO -->
	<div class="brand-info valign-wrapper">
		<div class="brand-info-text valign wow zoomIn" data-wow-duration="1s">
			<h2>brand overview</h2>
			<?php $brand_overview = apply_filters('the_content', $brand_overview); ?>
			<p><?php echo $brand_overview; ?></p>
		</div>
	</div>
	<!-- BRIEF -->
	<?php 
		$brief_image_url = get_post_meta(get_the_ID(), 'wpcf-brief-image', 1); 
	?>
	<?php 
		$brief_raw = get_post_meta(get_the_ID(), 'wpcf-brief', 1); 
		$brief = apply_filters('the_content', $brief_raw);
	?>
	<!--  DESKTOP  -->
	<div class="row valign-wrapper hide-on-med-and-down">
		<div class="col l6 s12 staff-member staff-member-image wow fadeInLeft" data-wow-duration="1s">
			<img alt="" width="100%" src="<?php echo $brief_image_url; ?>">
		</div>
		<div class="col l6 s12 valign staff-member staff-member-text wow fadeInRight" data-wow-duration="1s">
			<h2>Brief</h2>
			<p><?php echo $brief; ?></p>
		</div>
	</div>
	<!--  MOBILE  -->
	<div class="row hide-on-large-only">
		<div class="col l12 s12 staff-member staff-member-image" data-wow-duration="1s">
			<img alt="" width="100%" src="<?php echo $brief_image_url; ?>">
		</div>
		<div class="col l12 s12 valign staff-member staff-member-text " data-wow-duration="1s">
			<h2>Brief</h2>
			<p><?php echo $brief; ?></p>
		</div>
	</div>
	<!-- OBJECTIVES -->
	<?php 
		$objectives_image_url = get_post_meta(get_the_ID(), 'wpcf-objectives-image', 1); 
	?>
	<?php 
		$objectives_raw = get_post_meta(get_the_ID(), 'wpcf-objectives', 1); 
		$objectives = apply_filters('the_content', $objectives_raw);
	?>
	<!-- Desktop -->
	<div class="row valign-wrapper hide-on-med-and-down">
		<div class="col l6 valign staff-member staff-member-text wow fadeInLeft" data-wow-duration="1s">
			<h2>Objectives</h2>
			<p><?php echo $objectives; ?></p>
		</div>
		<div class="col l6 staff-member full-width-image staff-member-image wow fadeInRight" data-wow-duration="1s">
			<img alt="" width="100%" src="<?php echo $objectives_image_url; ?>">
		</div>
	</div>
	<!-- Mobile -->
	<div class="row hide-on-large-only">
		<div class="col s12 staff-member staff-member-image">
			<img alt="" width="100%" src="<?php echo $objectives_image_url; ?>">
		</div>
		<div class="col s12 staff-member staff-member-text wow fadeInLeft" data-wow-duration="1s">
			<h2>Objectives</h2>
			<p><?php echo $objectives; ?></p>
		</div>
	</div>

	<!-- SOLUTION -->
	<?php 
		$solution_image_url = get_post_meta(get_the_ID(), 'wpcf-solution-image', 1); 
	?>
	<?php 
		$solution_raw = get_post_meta(get_the_ID(), 'wpcf-solution', 1); 
		$solution = apply_filters('the_content', $solution_raw);
	?>
	<div class="row valign-wrapper hide-on-med-and-down">
		<div class="col l6 staff-member full-width-image staff-member-image wow fadeInLeft" data-wow-duration="1s">
			<img alt="" width="100%" src="<?php echo $solution_image_url; ?>">
		</div>
		<div class="col l6 valign staff-member staff-member-text wow fadeInRight" data-wow-duration="1s">
			<h2>Solution</h2>
			<?php $solution = apply_filters('the_content', $solution); ?>
			<p><?php echo $solution; ?></p>
		</div>
	</div>	

	<div class="row hide-on-large-only">
		<div class="col s12 staff-member full-width-image staff-member-image wow fadeInLeft" data-wow-duration="1s">
			<img alt="" width="100%" src="<?php echo $solution_image_url; ?>">
		</div>
		<div class="col s12 staff-member staff-member-text wow fadeInRight" data-wow-duration="1s">
			<h2>Solution</h2>
			<p><?php echo $solution; ?></p>
		</div>
	</div>
	<!-- EXECUTIONS -->
	<?php 
		$executions_raw = get_post_meta(get_the_ID(), 'wpcf-executions', 1); 
		$executions = apply_filters('the_content', $executions_raw);
	?>
	<?php 
		$gallery_1 = get_post_meta(get_the_ID(), 'wpcf-gallery-1', 1); 
		$gallery_2 = get_post_meta(get_the_ID(), 'wpcf-gallery-2', 1); 
		$gallery_3 = get_post_meta(get_the_ID(), 'wpcf-gallery-3', 1); 
		$gallery_4 = get_post_meta(get_the_ID(), 'wpcf-gallery-4', 1); 
		$gallery_5 = get_post_meta(get_the_ID(), 'wpcf-gallery-5', 1); 
	?>
	<div>
		<div class="executions-info valign-wrapper">
			<div class="executions-info-text valign wow fadeIn">
				<h2>Executions</h2>
				<p><?php echo $executions; ?></p>
			</div>
		</div>

		<div class="row">
			<div class="col l4 s12 full-width-image wow fadeInLeft" data-wow-duration="1s">
				<img src="<?php echo $gallery_1; ?>">
			</div>
			<div class="col l4 s12 full-width-image wow fadeIn" data-wow-duration="1s">
				<img src="<?php echo $gallery_2; ?>">
			</div>
			<div class="col l4 s12 full-width-image wow fadeInRight" data-wow-duration="1s">
				<img src="<?php echo $gallery_3; ?>">
			</div>
		</div>
		<div class="row">
			<div class="col l4 s12 full-width-image wow fadeInLeft" data-wow-duration="1s">
				<img src="<?php echo $gallery_4; ?>">
			</div>
			<div class="col l8 s12 full-width-image wow fadeInRight" data-wow-duration="1s">
				<img src="<?php echo $gallery_5; ?>">
			</div>
		</div>
	</div>
	<!-- RESULTS -->

	<?php 
		$results_raw = get_post_meta(get_the_ID(), 'wpcf-results', 1); 
		$results = apply_filters('the_content', $results_raw);
	?>
	<div id="fourthsection" class="results">
		<div class="results-info valign-wrapper">
			<div class="results-info-text valign wow fadeIn" data-wow-duration="1s">
				<h2>Results</h2>
				<p><?php echo $results?></p>
			</div>
		</div>
		<div class="row">
			<?php 
				$numberone = get_post_meta(get_the_ID(), 'wpcf-1st-number', 1); 
			?>
			<?php 
				$numberonedesc_raw = get_post_meta(get_the_ID(), 'wpcf-1st-number-description', 1); 
				$numberonedesc = apply_filters('the_content', $numberonedesc_raw);
			?>
			<div class="col l4 s12  wow fadeInLeft"  data-wow-duration="1s">
				<h2 id="odometerone" class="odometer" data-value="<?php echo $numberone; ?>">000</h2>
				<p><?php echo $numberonedesc?></p>
			</div>
			<?php 
				$numbertwo = get_post_meta(get_the_ID(), 'wpcf-2nd-number', 1); 
			?>
			<?php 
				$numbertwodesc_raw = get_post_meta(get_the_ID(), 'wpcf-2nd-number-description', 1); 
				$numbertwodesc = apply_filters('the_content', $numbertwodesc_raw);
			?>
			<div class="col l4 s12 wow fadeIn" data-wow-duration="1s">
				<h2 id="odometertwo" class="odometer" data-value="<?php echo $numbertwo; ?>">000</h2>
				<p><?php echo $numbertwodesc?></p>
			</div>
			<?php 
				$numberthree = get_post_meta(get_the_ID(), 'wpcf-3rd-number', 1); 
			?>
			<?php 
				$numberthreedesc_raw = get_post_meta(get_the_ID(), 'wpcf-3rd-number-description', 1); 
				$numberthreedesc = apply_filters('the_content', $numberthreedesc_raw);
			?>
			<div class="col l4 s12 wow fadeInRight" data-wow-duration="1s">
				<h2 id="odometerthree" data-value="<?php echo $numberthree; ?>" class="odometer">000</h2>
				<p><?php echo $numberthreedesc?></p>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>