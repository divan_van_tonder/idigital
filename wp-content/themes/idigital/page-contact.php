<?php
/**
 * Template name: Contact
 *
 */
get_header(); ?>
<div id="fp-nav" class="right" style="margin-top: -33.5px;">
	<ul id="myMenu">
    <li data-menuanchor="firstPage" class="active"><a href="#firstPage"><span></span></a></li>
    <li data-menuanchor="secondPage"><a href="#secondPage"><span></span></a></li>
    <li data-menuanchor="footer"><a href="#footer"><span></span></a></li>
</ul>
</div>

<div id="fullpage">
	<div class="section" id="#firspage" style="position:relative;">

	<?php $video_url = get_post_meta(get_the_ID(), 'wpcf-bg-video', 1); ?>
	<?php $image_url = get_post_meta(get_the_ID(), 'wpcf-bg-image', 1); ?>
	<?php $shortcode = get_post_meta(get_the_ID(), 'wpcf-shortcode', 1); ?>

	<?php if ($video_url != "") { ?>
		<video autoplay id="bgvid" loop>
			<source src="<?php echo $video_url; ?>" />
		</video>
	<?php } else {?>
		<img class="video-placeholder" src="<?php echo $image_url; ?>">
	<?php } ?> 

		<div class="container">
			<div class="row valign-wrapper">
				<div class="col l8 offset-l2 valign">
					<h1><?php the_title(); ?></h1>
					<?php if (have_posts()) :
							   while (have_posts()) :
							      the_post();
							         the_content();
							   endwhile;
							endif;?>
					<a target="_blank" href="https://www.google.com/maps/dir/current+location/Buitenkloof+Studios,+8+Kloof+Street,+Gardens,+Cape+Town,+8001,+South+Africa/" class="waves-effect waves-dark btn btn-large z-depth-0">
	      		Get Directions
	      		<span class="btn-border vert left"></span>
						<span class="btn-border vert right"></span>
						<span class="btn-border hor top"></span>
						<span class="btn-border hor bottom"></span>
	      	</a>
					<div class="social-icons">
				    <a href="https://www.facebook.com/iDigitalStrategySA" target="_blank" alt="IDIGITAL social media icon">
				      <i class="fa fa-facebook" aria-hidden="true"></i>
				    </a>
				    <a href="https://www.instagram.com/idigital.io/" target="_blank" alt="IDIGITAL social media icon">
				      <i class="fa fa-instagram" aria-hidden="true"></i>
				    </a>
				    <a href="https://twitter.com/IDIGITALio" target="_blank" alt="IDIGITAL social media icon">
				      <i class="fa fa-twitter" aria-hidden="true"></i>
				    </a>
				    <a href="https://www.snapchat.com/add/idigital.io" target="_blank" alt="IDIGITAL social media icon">
				      <i class="fa fa-snapchat-ghost" aria-hidden="true"></i>
				    </a>
				    <a href="http://www.linkedin.com/company/idigitalio" target="_blank" alt="IDIGITAL social media icon">
				      <i class="fa fa-linkedin" aria-hidden="true"></i>
				    </a>
				  </div>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row valign-wrapper">
				<div class="container">
					<div class="col l8 offset-l2 s12 valign">
						<?php echo do_shortcode( $shortcode ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer();
