<!DOCTYPE html>
<!--
     _     ___ ____ _   _ _____ ______        _____ _____ ____ _   _
    | |   |_ _/ ___| | | |_   _/ ___\ \      / /_ _|_   _/ ___| | | |
    | |    | | |  _| |_| | | | \___ \\ \ /\ / / | |  | || |   | |_| |
    | |___ | | |_| |  _  | | |  ___) |\ V  V /  | |  | || |___|  _  |
    |_____|___\____|_| |_| |_| |____/  \_/\_/  |___| |_| \____|_| |_|

    Website developed and maintaind by LIGHTSWITCH. info_at_lightswitch.solutions

-->
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fullpage.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
  <link rel="stylesheet" href="http://github.hubspot.com/odometer/themes/odometer-theme-minimal.css">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class="loading">
    <img src="<?php echo get_template_directory_uri(); ?>/images/loading.gif">
  </div>

  <div class="header">
    <a class="header-img" href="/" style=""></a>
    <button class="c-hamburger c-hamburger--htx nav-trigger" id="nav-trigger">
      <span></span>
    </button>
  </div>

  <ul class="navigation">
    <li class="nav-item"><a href="/">Home</a></li>
    <li class="nav-item"><a href="/about">About</a></li>
    <li class="nav-item"><a href="/services">Services</a></li>
    <li class="nav-item"><a href="/work">Work</a></li>
    <li class="nav-item"><a href="/contact">Contact</a></li>
    <div class="social-icons menu">
      <a href="https://www.facebook.com/iDigitalStrategySA" target="_blank" alt="IDIGITAL social media icon">
        <i class="fa fa-facebook" aria-hidden="true"></i>
      </a>
      <a href="https://www.instagram.com/idigital.io/" target="_blank" alt="IDIGITAL social media icon">
        <i class="fa fa-instagram" aria-hidden="true"></i>
      </a>
      <a href="https://twitter.com/IDIGITALio" target="_blank" alt="IDIGITAL social media icon">
        <i class="fa fa-twitter" aria-hidden="true"></i>
      </a>
      <a href="https://www.snapchat.com/add/idigital.io" target="_blank" alt="IDIGITAL social media icon">
        <i class="fa fa-snapchat-ghost" aria-hidden="true"></i>
      </a>
      <a href="http://www.linkedin.com/company/idigitalio" target="_blank" alt="IDIGITAL social media icon">
        <i class="fa fa-linkedin" aria-hidden="true"></i>
      </a>
    </div>
  </ul>


  <div class="site-wrap">
    
