<?php
/**
 * Template name: Work
 *
 */
get_header(); ?>

<div style="height: 100vh; position:relative;" class="section valign-wrapper">
	<?php $video_url = get_post_meta(get_the_ID(), 'wpcf-bg-video', 1); ?>
	<?php $image_url = get_post_meta(get_the_ID(), 'wpcf-bg-image', 1); ?>

	<?php if ($video_url != "") { ?>
		<video autoplay id="bgvid" loop>
			<source src="<?php echo $video_url; ?>" />
		</video>
	<?php } else {?>
		<img class="video-placeholder" src="<?php echo $image_url; ?>">
	<?php } ?> 
	<div class="container">
		<div class="row">
			<div class="col l8 offset-l2 valign">
					<?php 
					if ( have_posts() ) : 
						while ( have_posts() ) : the_post(); 
					?>
					<h1><?php the_title(); ?></h1>
					<?php
					    the_content();
					  endwhile; 
					endif;
					?>
					<div class="row hide-on-med-and-down">
						<section class="slick-slider">
							<?php 
								$clientlogoone = get_post_meta(get_the_ID(), 'wpcf-client-logos-1', 1); 
								$clientlogotwo = get_post_meta(get_the_ID(), 'wpcf-client-logos-2', 1); 
								$clientlogothree = get_post_meta(get_the_ID(), 'wpcf-client-logos-3', 1); 
								$clientlogofour = get_post_meta(get_the_ID(), 'wpcf-client-logos-4', 1); 
								$clientlogofive = get_post_meta(get_the_ID(), 'wpcf-client-logos-5', 1); 
								$clientlogosix = get_post_meta(get_the_ID(), 'wpcf-client-logos-6', 1);
								$clientlogoseven = get_post_meta(get_the_ID(), 'wpcf-client-logos-7', 1); 
								$clientlogoeight = get_post_meta(get_the_ID(), 'wpcf-client-logos-8', 1); 
								$clientlogonine = get_post_meta(get_the_ID(), 'wpcf-client-logos-9', 1); 
								$clientlogoten = get_post_meta(get_the_ID(), 'wpcf-client-logos-10', 1); 
								$clientlogoeleven = get_post_meta(get_the_ID(), 'wpcf-client-logos-11', 1); 
								$clientlogotwelve = get_post_meta(get_the_ID(), 'wpcf-client-logos-12', 1);
								$clientlogolinkone = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-1', 1); 
								$clientlogolinktwo = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-2', 1); 
								$clientlogolinkthree = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-3', 1); 
								$clientlogolinkfour = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-4', 1); 
								$clientlogolinkfive = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-5', 1); 
								$clientlogolinksix = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-6', 1); 
								$clientlogolinkseven = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-7', 1); 
								$clientlogolinkeight = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-8', 1); 
								$clientlogolinknine = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-9', 1); 
								$clientlogolinkten = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-10', 1); 
								$clientlogolinkeleven = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-11', 1); 
								$clientlogolinktwelve = get_post_meta(get_the_ID(), 'wpcf-client-logos-link-12', 1); 
							?>

							<?php if ($clientlogolinkone) { ?>
								<div class="col l2 s4">
										<a href="<?php echo $clientlogolinkone; ?>">
										<img width="135" height="135" src="<?php echo $clientlogoone; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinktwo) { ?>
								<div class="col l2 s4">
										<a href="<?php echo $clientlogolinktwo; ?>">
										<img width="135" height="135" src="<?php echo $clientlogotwo; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinkthree) { ?>
								<div class="col l2 s4">
										<a href="<?php echo $clientlogolinkthree; ?>">
										<img width="135" height="135" src="<?php echo $clientlogothree; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogoslinkfour) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogoslinkfour; ?>">
										<img width="135" height="135" src="<?php echo $clientlogofour; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinkfive) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinkfive; ?>">
										<img width="135" height="135" src="<?php echo $clientlogofive; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinksix) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinksix; ?>">
										<img width="135" height="135" src="<?php echo $clientlogosix; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinkseven) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinkseven; ?>">
										<img width="135" height="135" src="<?php echo $clientlogoseven; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinkeight) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinkeight; ?>">
										<img width="135" height="135" src="<?php echo $clientlogoeight; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinknine) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinknine; ?>">
										<img width="135" height="135" src="<?php echo $clientlogonine; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinkten) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinkten; ?>">
										<img width="135" height="135" src="<?php echo $clientlogoten; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinkeleven) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinkeleven; ?>">
										<img width="135" height="135" src="<?php echo $clientlogoeleven; ?>">
									</a>
								</div>
							<?php } ?>
							<?php if ($clientlogolinktwelve) { ?>
								<div class="col l2 s4 hide-on-med-and-down">
										<a href="<?php echo $clientlogolinktwelve; ?>">
										<img width="135" height="135" src="<?php echo $clientlogotwelve; ?>">
									</a>
								</div>
							<?php } ?>
						</section>
					</div>
			</div>
		</div>
	</div>
	<div class="row down-arrow">
		<div class="col l12 s12">
			<a href="#bios">
				<img src="/wp-content/themes/idigital/images/chevron-down.svg">
			</a>
		</div>
	</div>
</div>

<?php 
	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'case-study',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'post_status'      => 'publish'
	);
	$case_studies = get_posts( $args); 
?>


<div id="bios">
	<div class="row case-study">
		<?php 
			foreach ($case_studies as $key => $case_study_val): 
				$image_url = wp_get_attachment_url( get_post_thumbnail_id($case_study_val->ID) );
		?>
		<div class="col l6 case-study">
			<div class="case-study-text">
				<img src="<?php echo $image_url; ?>">
				<div class="picture-overlay valign-wrapper">
					<div class="center-align valign">
						<h2><?php echo $case_study_val->post_title; ?></h2>
						<p><?php echo $case_study_val->post_content; ?></p>
						<a href="<?php echo $case_study_val->post_name; ?>" class="waves-effect waves-dark btn btn-large z-depth-0">
	      			View More
		      		<span class="btn-border vert left"></span>
							<span class="btn-border vert right"></span>
							<span class="btn-border hor top"></span>
							<span class="btn-border hor bottom"></span>
		      	</a>

					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>	
	</div>
</div>

<?php get_footer();
