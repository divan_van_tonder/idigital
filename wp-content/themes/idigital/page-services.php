<?php
/**
 * Template name: Services
 *
 */
get_header(); ?>

<?php 
	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'service',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'post_status'      => 'publish'
	);
	$service_slides = get_posts( $args); 
?>

<div id="fp-nav" class="right" style="margin-top: -33.5px;">
	<ul id="myMenu">
		<?php foreach ($service_slides as $key => $service_slide_val): ?>
		  <li data-menuanchor="Page-<?php echo $service_slide_val->ID; ?>" class="active"><a href="#Page-<?php echo $service_slide_val->ID; ?>"><span></span></a></li>
		<?php endforeach; ?>
    <li data-menuanchor="footer"><a href="#footer"><span></span></a></li>
	</ul>
</div>

<div id="fullpage">
	<?php 
		$pageNumber = 1;
		foreach ($service_slides as $key => $service_slide_val): 
			$pageNumber++;
	?>
	
	<div class="section" style="position:relative; background:transparent;">

		<?php $video_url = get_post_meta($service_slide_val->ID, 'wpcf-bg-video', 1); ?>
		<?php $image_url = get_post_meta($service_slide_val->ID, 'wpcf-bg-image', 1); ?>

		<?php if ($video_url != "") { ?>
			<video autoplay id="bgvid" loop>
				<source src="<?php echo $video_url; ?>" />
			</video>
		<?php } else {?>
			<img class="video-placeholder" src="<?php echo $image_url; ?>">
		<?php } ?> 

		<div class="container">
			<div class="row valign-wrapper">
				<div class="container">
					<div class="col l12 s12 valign">
						<h1><?php echo $service_slide_val->post_title; ?></h1>
						<?php $service_slide_val->post_content = apply_filters('the_content', $service_slide_val->post_content); ?>
						<p><?php echo $service_slide_val->post_content; ?></p>
						<?php 
							if ($service_slide_val->post_excerpt !== ""){?>
							<a href="<?php echo $service_slide_val->post_excerpt; ?>" class="waves-effect waves-dark btn btn-large z-depth-0">
			      		Case Study
			      		<span class="btn-border vert left"></span>
								<span class="btn-border vert right"></span>
								<span class="btn-border hor top"></span>
								<span class="btn-border hor bottom"></span>
			      	</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row down-arrow">
			<div class="col l12 s12">
				<?php if ($pageNumber > count($service_slides)) { ?>
					<a href="#" onclick="$.fn.fullpage.moveTo(1);">
						<img src="/wp-content/themes/idigital/images/chevron-up.svg">
					</a>
				<?php } else { ?>
					<a href="#" onclick="$.fn.fullpage.moveTo(<?php echo $pageNumber; ?>);">
						<img src="/wp-content/themes/idigital/images/chevron-down.svg">
					</a>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php endforeach; ?>	

<?php get_footer(); ?>