<?php
/**
 * Template name: About
 *
 */
get_header(); ?>
<!-- FIRST PAGE -->
<div style="height: 100vh; position:relative;" class="section valign-wrapper">

	<?php $video_url = get_post_meta(get_the_ID(), 'wpcf-bg-video', 1); ?>
	<?php $image_url = get_post_meta(get_the_ID(), 'wpcf-bg-image', 1); ?>

	<?php if ($video_url != "") { ?>
		<video autoplay id="bgvid" loop>
			<source src="<?php echo $video_url; ?>" />
		</video>
	<?php } else {?>
		<img class="video-placeholder" src="<?php echo $image_url; ?>">
	<?php } ?> 

	<div class="container">
		<div class="row">
			<div class="col l12 valign fullwidth">
				<?php 
					if ( have_posts() ) : 
						while ( have_posts() ) : the_post(); 
					    the_content();
					  endwhile; 
					endif;
				?>
			</div>
		</div>
	</div>
	<div class="row down-arrow">
		<div class="col l12 s12">
			<a href="#bios">
				<img src="/wp-content/themes/idigital/images/chevron-down.svg">
			</a>
		</div>
	</div>
</div>
<!--  END FIRST PAGE -->

<?php 
	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'staff-member',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'post_status'      => 'publish'
	);
	$staff_members = get_posts( $args); 
?>


<div id="bios">
	<div class="row case-study">
		<?php 
			foreach ($staff_members as $key => $staff_member_val): 
				$image_url = wp_get_attachment_url( get_post_thumbnail_id($staff_member_val->ID) );
		?>
		<div class="col l6 case-study">
			<div class="case-study-text">
				<img alt="<?php echo $staff_member_val->post_title; ?>"width="100%" src="<?php echo $image_url; ?>">
				<div class="picture-overlay valign-wrapper">
					<div class="center-align valign">
						<h2><?php echo $staff_member_val->post_title; ?></h2>
						<p><?php echo $staff_member_val->post_content; ?></p>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>	
	</div>
</div>

<?php get_footer();?>