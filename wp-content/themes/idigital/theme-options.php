<div class="wrap">
<h2>Settings</h2>

<form method="post" action="options.php">
    <?php settings_fields('idigital-settings-group'); ?>
    <?php do_settings_sections('idigital-settings-group'); ?>
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Google Analytics Code</th>
            <td><input type="text" name="idigital_google_code" class="regular-text" value="<?php echo get_option('idigital_google_code'); ?>" /></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
