<?php
/**
 * Template name: Full screen
 *
 */
get_header(); ?>

<?php 
	$args = array(
		'posts_per_page'   => -1,
		'post_type'        => 'homepage-slide',
		'orderby'          => 'date',
		'order'            => 'ASC',
		'post_status'      => 'publish'
	);
	$homepage_slides = get_posts( $args);
?>

<div id="fp-nav" class="right" style="margin-top: -33.5px;">
	<ul id="myMenu">
		<?php foreach ($homepage_slides as $key => $homepage_slide_val): ?>
			<li data-menuanchor="Page-<?php echo $homepage_slide_val->ID; ?>" class="active"><a href="#Page-<?php echo $homepage_slide_val->ID; ?>"><span></span></a></li>
		<?php endforeach; ?>
		<li data-menuanchor="footer"><a href="#footer"><span></span></a></li>
	</ul>
</div>

<div id="fullpage">
	<?php 
		$pageNumber = 1;
		foreach ($homepage_slides as $key => $homepage_slide_val): 
			$pageNumber++;
	?>

	<div class="section" style="position:relative; background:transparent;">

		<?php $homepage_video_url = get_post_meta($homepage_slide_val->ID, 'wpcf-bg-video', 1); ?>
		<?php $homepage_image_url = get_post_meta($homepage_slide_val->ID, 'wpcf-bg-image', 1); ?>

		<?php if ($homepage_video_url != "") { ?>
			<video autoplay id="bgvid" loop autoplay="autoplay" loop="loop">
				<source src="<?php echo $homepage_video_url; ?>" />
			</video>
		<?php } else { ?>
			<img class="video-placeholder" src="<?php echo $homepage_image_url; ?>">
		<?php } ?> 

		<div class="container">
			<div class="row valign-wrapper">
				<div class="container">
					<div class="col l12 s12 valign">
						<h1><?php echo $homepage_slide_val->post_title; ?></h1>
						<?php $homepage_slide_val->post_content = apply_filters('the_content', $homepage_slide_val->post_content); ?>
						<p><?php echo $homepage_slide_val->post_content; ?></p>
						<?php 
							if ($homepage_slide_val->post_excerpt !== ""){?>
							<a href="<?php echo $homepage_slide_val->post_excerpt; ?>" class="waves-effect waves-dark btn btn-large z-depth-0">
			      		Read More
			      		<span class="btn-border vert left"></span>
								<span class="btn-border vert right"></span>
								<span class="btn-border hor top"></span>
								<span class="btn-border hor bottom"></span>
			      	</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row down-arrow">
			<div class="col l12 s12">
				<?php if ($pageNumber > count($homepage_slides)) { ?>
					<a href="#" onclick="$.fn.fullpage.moveTo(1);">
						<img src="/wp-content/themes/idigital/images/chevron-up.svg">
					</a>
				<?php } else { ?>
					<a href="#" onclick="$.fn.fullpage.moveTo(<?php echo $pageNumber; ?>);">
						<img src="/wp-content/themes/idigital/images/chevron-down.svg">
					</a>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php endforeach; ?>	

<?php get_footer(); ?>